package com.persistenceApi.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.persistenceApi.models.Customer;
import com.persistenceApi.services.CustomerService;

@SessionAttributes("customer") // Keeps attributes like "ID" in session till it is set as completed
@Controller
@RequestMapping("/customers")
public class CustomerController {

	private static final String SAVE_EDIT_FORM = "saveCustomerForm";

	@Value("${global.appTitle}")
	private String appTitle;

	@Value("${customer.header}")
	private String header;

	@Value("${customer.saveForm.header}")
	private String saveFormHeader;

	@Value("${customer.editForm.header}")
	private String editFormHeader;

	@Autowired
	private CustomerService customerService;

	@GetMapping(value = { "/list" })
	public String listCustomers(Model model) {
		model.addAttribute("title", appTitle);
		model.addAttribute("header", header);
		model.addAttribute("customersList", customerService.findAll());
		return "customers";
	}

	@GetMapping(value = "/save")
	public String createCustomerFromForm(Model model) {
		Customer c = new Customer();
		model.addAttribute("title", appTitle);
		model.addAttribute("header", saveFormHeader);
		model.addAttribute("customer", c);
		return SAVE_EDIT_FORM;
	}

	// This method will redirect to "list" GetMapping path
	// @Valid performs all the validations from the used validation annotations
	// @Valid and BindingResult MUST BE DECLARED TOGETHER
	@PostMapping(value = "/save")
	public String saveCustomer(@Valid Customer customer, BindingResult validations, Model model,
			SessionStatus session) {
		if (validations.hasErrors()) {
			model.addAttribute("title", appTitle);
			model.addAttribute("header", saveFormHeader);
			return SAVE_EDIT_FORM;
		}
		customerService.save(customer);
		session.setComplete(); // After Persist or Merge we set the session that contains the current Bean as
								// completed, this won�t keep session data anymore
		return "redirect:list";
	}

	@GetMapping(value = "/save/{id}")
	public String edit(@PathVariable Long id, Model model) {
		Customer c = null;
		if (id > 0) {
			c = customerService.findOne(id);
		} else {
			return "redirect:list";
		}
		model.addAttribute("title", appTitle);
		model.addAttribute("header", editFormHeader);
		model.addAttribute("customer", c);
		return SAVE_EDIT_FORM;
	}

	@GetMapping(value = "/delete/{id}")
	public String delete(@PathVariable Long id) {
		if (id > 0) {
			customerService.delete(id);
		}
		return "redirect:/customers/list";
	}

}
