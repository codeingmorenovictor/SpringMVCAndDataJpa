package com.persistenceApi.servicesImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.persistenceApi.models.Customer;
import com.persistenceApi.services.CustomerService;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class CustomerServiceImpl implements CustomerService {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Customer> findAll() {
		return em.createQuery("from Customer").getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Customer findOne(Long id) {
		return em.find(Customer.class, id);
	}

	@Override
	@Transactional
	public void save(Customer customer) {
		if (customer.getId() != null && customer.getId() > 0) {
			em.merge(customer);
		} else {
			try {
				em.persist(customer);
			} catch (Exception e) {
				log.info("ERROR IN PERSIST::::: {}", e.getMessage());
			}
		}
	}

	@Override
	@Transactional
	public void delete(Long id) {
		em.remove(findOne(id));
	}

}
